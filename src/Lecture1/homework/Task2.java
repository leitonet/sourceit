package Lecture1.homework;

/**
 * Created by alexeyburkun on 2/26/17.
 */
public class Task2 {
    public static void main(String[] args) {
        int merquri = 58_000_000;
        int venera = 108_200_000;
        int mars = 227_900_000;
        int earth = 149_600_000;
        System.out.println("Merquri: " + merquri + "km to Sun");
        System.out.println("Venera: " + venera + "km to Sun");
        System.out.println("Mars: " + mars + "km to Sun");
        System.out.println("Earth: " + earth + "km to Sun");
        System.out.println("From Merquri to Earth: " + (earth - merquri) + "km");
        System.out.println("From Venera to Earth: " + (earth - venera) + "km");
        System.out.println("From Mars to Earth: " + (mars - earth) + "km");
    }
}
