package Lecture1.homework;

/**
 * Created by alexeyburkun on 2/26/17.
 */
public class Task1 {
    public static void main(String[] args) {
        String india = "India";
        String china = "China";
        String usa = "USA";
        int a = 1_187_550_000;
        int b = 1_339_450_000;
        int c = 310_241_000;
        System.out.println(india + ": " + a + " peoples");
        System.out.println(china + ": " + b + " peoples");
        System.out.println(usa + ": " + c + " peoples");
    }
}
