package Lecture1.homework;

/**
 * Created by alexeyburkun on 2/26/17.
 */
public class Task4 {
    public static void main(String[] args) {
        double usd = 26.777;
        double eur = 28.081;
        double rub = 0.442;
        System.out.println("5000 грн. в долларах: " + (5000 / usd));
        System.out.println("5000 грн. в евро: " + (5000 / eur));
        System.out.println("5000 грн. в рублях: " + (5000 / rub));
    }
}
