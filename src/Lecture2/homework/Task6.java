package Lecture2.homework;
//6* Из текста удалить все слова заданной длины, начинающиеся на согласную букву.

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Task6 {

    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    static String inputText;
    static int length;
    static ArrayList<String> wordsArray = new ArrayList<>();
    static ArrayList<String> deletedWordsArray = new ArrayList<>();

    public static void main(String[] args)throws IOException {
        System.err.println("Введите текст:");
        inputText = reader.readLine();
        System.err.println("Задайте длинну слова:");
        length = Integer.parseInt(reader.readLine());
        deleteWords();
    }

    //удаляю все слова заданной длины, начинающиеся на согласную букву.
    public static void deleteWords(){
        for(String s : inputText.split(" ")){
            wordsArray.add(s);
        }

        for(String s : wordsArray){
            s = s.toLowerCase();
            if (s.contains(",")){
                s = s.replace("," , "");
            }
            if(s.length()== length){
                if(s.startsWith("б") || s.startsWith("в") || s.startsWith("г") || s.startsWith("д") || s.startsWith("ж") || s.startsWith("з") || s.startsWith("к") ||
                        s.startsWith("л") || s.startsWith("м") || s.startsWith("н") || s.startsWith("п") || s.startsWith("р") || s.startsWith("с") || s.startsWith("т") ||
                        s.startsWith("ф") || s.startsWith("х") || s.startsWith("ц") || s.startsWith("ч") || s.startsWith("ш") || s.startsWith("щ")){

                }else {deletedWordsArray.add(s);}
            }else {deletedWordsArray.add(s);
            }
        }
        System.err.println("Измененный текст:");

        for(String s : deletedWordsArray){
            System.out.print(s + " ");
        }
    }
}
