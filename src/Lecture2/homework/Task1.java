package Lecture2.homework;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task1 {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите первое число: ");
        int innerNamber1 = Integer.parseInt(reader.readLine());
        System.out.println("Введите второе число: ");
        int innerNamber2 = Integer.parseInt(reader.readLine());
        System.out.println("Введите третье число: ");
        int innerNamber3 = Integer.parseInt(reader.readLine());
        int maxNumber1;
        int maxNumber2;

        if (innerNamber1 > innerNamber2) {
            maxNumber1 = innerNamber1;
        } else {
            maxNumber1 = innerNamber2;
        }

        if (innerNamber2 > innerNamber3) {
            maxNumber2 = innerNamber2;
        } else {
            maxNumber2 = innerNamber3;
        }

        System.out.println("Сумма квадратов двух наибольших чисел = " + (int) (Math.pow(maxNumber1, 2) + Math.pow(maxNumber2, 2)));

    }
}
