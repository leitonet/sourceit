package Lecture2.homework;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task2 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите а:");
        int a = Integer.parseInt(reader.readLine());
        System.out.println("Введите b:");
        int b = Integer.parseInt(reader.readLine());
        System.out.println("Введите c:");
        int c = Integer.parseInt(reader.readLine());
        int d = b * b - 4 * a * c;
        if(d > 0){
            double x1, x2;
            x1 = (-b - Math.sqrt(d))/(2 * a);
            x2 = (-b + Math.sqrt(d))/(2 * a);
            System.out.println("Корни уравнения: Х1 = " + x1 + ", x2 = " + x2);
        }else if(d == 0){
            double x = -b / (2 * a);
            System.out.println("Уравнение имеет единственный корень: Х = " + x);
        }else {
            System.out.println("Уравнение не имеет действительных корней!");
        }

    }
}
