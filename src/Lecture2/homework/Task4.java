package Lecture2.homework;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Task4 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String string = "";
        int max = 0;
        int min = 0;

        ArrayList<String> arrayStrings = new ArrayList<>();

        while (!string.equals("go")){
            System.out.println("Введите строку или go для расчета:");
            arrayStrings.add(string = reader.readLine());
        }

        arrayStrings.remove("go");
        min = arrayStrings.get(0).length();

        for (String str : arrayStrings){
            if (str.length() > max){
                max = str.length();
            }
            if (str.length() < min){
                min = str.length();
            }
        }

        for (String str : arrayStrings) {
            if(str.length() < ((max - min)/2)){
                System.out.println(str + " " + str.length());
            }
        }



    }
}
