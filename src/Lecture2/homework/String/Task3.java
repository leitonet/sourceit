package Lecture2.homework.String;
//3. Ввести n строк с консоли. Найти количество слов, содержащих только символы латинского алфавита,
//      а среди них – количество слов с равным числом гласных и согласных букв.


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Task3 {

    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    static ArrayList<String> inputTextArray = new ArrayList<>();
    static ArrayList<String> onlyLatinasWordsArray = new ArrayList<>();
    static String tempString = "";
    static int numberLatinaWords;
    static int count = 0;
    static int countVowels = 0;
    static int countConsonants = 0;
    static int countEqualsConsonantAndVowel = 0;

    public static void main(String[] args) throws IOException {
        inputText();
        findLatineSymbols();
        System.out.println(numberLatinaWords);
        findVowelsAndConsonants();
        System.out.println("Количество слов с равным числом гласных и согласных букв: " + countEqualsConsonantAndVowel);

    }

    //Ввожу строки
    public static void inputText() throws IOException {
        do {
            tempString = reader.readLine();
            inputTextArray.add(tempString);
        } while (!tempString.equals("end"));
        inputTextArray.remove("end");
    }

    //Нахожу количество слов, содержащих только символы латинского алфавита
    public static void findLatineSymbols() {
        for (int i = 0; i < inputTextArray.size(); i++) {
            count = 0;
            for (char tmp : inputTextArray.get(i).toCharArray()) {
                String str = "" + tmp;
                if (("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ").contains(str)) {

                } else {
                    count++;
                }
            }
            if (count == 0) {
                onlyLatinasWordsArray.add(inputTextArray.get(i));
            }
        }
        numberLatinaWords = onlyLatinasWordsArray.size();
    }

    //Нахожу количество слов с равным числом гласных и согласных букв
    public static void findVowelsAndConsonants (){
        for(int i = 0; i < onlyLatinasWordsArray.size(); i++){
            countVowels = 0;
            countConsonants = 0;
            for(char ch : onlyLatinasWordsArray.get(i).toCharArray()){
                String str = "" + ch;
                if(("bcdfghjklmnpqrstvwxzBCDFGHJKLMNPQRSTVWXZ").contains(str)){
                    countConsonants++;
                }else{
                    countVowels++;
                }
            }
            if(countConsonants == countVowels){
                countEqualsConsonantAndVowel++;
            }
        }
    }
}