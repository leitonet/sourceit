package Lecture2.homework.String;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Task5 {
    static ArrayList<String> inputTextArray = new ArrayList<>();
    static ArrayList<String> wordsPalinromsArray = new ArrayList<>();
    static String inputText;
    static char [] tempReverse;
    static char [] temp;
    static boolean palindrome = true;

    public static void main(String[] args)throws IOException {
        inputText();
        checkOnPalindrome();
    }

    //Ввожу текст
    public static void inputText() throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        inputText = reader.readLine();
        inputText = inputText.toLowerCase();
        for (String s : inputText.split(" ")){
            for(char ch : s.toCharArray()){
                String tmp = "" + ch;
                if("!?.\"-'".contains(tmp)){
                    s = s.replace(tmp, "");
                }
            }
            inputTextArray.add(s);
        }
    }

    //Проверяю является ли слово полиндромом
    public static void checkOnPalindrome(){
        for(String s : inputTextArray){
            tempReverse = new StringBuilder(s).reverse().toString().toCharArray();
            temp = s.toCharArray();
            palindrome = true;
            for(int i = 0; i < temp.length; i++){
                String word ="" + temp[i];
                String wordReverse ="" + tempReverse[i];
                if(word.equals(wordReverse)){

                }else {
                    palindrome = false;
                    System.out.println("Слово \"" + s + "\" не является палиндромом");
                    break;
                }
            }
            if (palindrome){
                System.out.println("Слово \"" + s + "\" - палиндромом");
            }
        }
    }
}
