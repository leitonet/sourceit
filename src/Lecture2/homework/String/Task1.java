package Lecture2.homework.String;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task1 {
    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    static String inputText;
    static String longestWord = "";
    static String sentenceWithLongestWord;
    static String longestSentence = "";
    static String [] wordsArray;
    static String [] sentenceArray;
    static int countSentence = 0;

    public static void main(String[] args) throws IOException {
        System.err.println("Введите текст:");
        inputText = reader.readLine();
        countingSentences();
        viewLongerSentence();
        viewLongerWord();
    }
    //Подсчитываю количество предложений
    public static void countingSentences(){
        wordsArray = inputText.split(" ");
        for(String s : wordsArray){
            if(s.contains(".") || s.contains("!") || s.contains("?")){
                countSentence ++;
            }
        }
        System.out.println("Количество предложений: " + countSentence);
    }

    //Вывожу предложение с максимальным количеством слов
    public static void viewLongerSentence(){
        sentenceArray = inputText.split("\\.");
        for(String s : sentenceArray){
            if(s.length() > longestSentence.length()){
                longestSentence = s;
            }
        }
        System.err.println("Предложение с максимальным количеством слов: ");
        System.out.println(longestSentence);
    }

    //Вывожу предложение, которое одержит слово с максимальным количеством букв
    public static void viewLongerWord(){
        for(String s : sentenceArray){
            for(String str : s.split(" ")){
                if(str.length() > longestWord.length()){
                    longestWord = str;
                }
            }
        }
        for(String s : sentenceArray){
            if(s.contains(longestWord)){
                System.err.println("Предложение, которое одержит слово с максимальным количеством букв: " );
                System.out.println(s);
            }
        }
    }
}
