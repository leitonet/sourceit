package Lecture2.homework.String;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Task2 {
    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    static ArrayList<String> inputTextArray = new ArrayList<>();
    static ArrayList<String> wordsArray = new ArrayList<>();
    static ArrayList<Character> tempCharsArray = new ArrayList<>();
    static String tempString = "";
    static int count = 0;

    public static void main(String[] args) throws IOException {
        inputText();
        findWordsWithNonMatches();
        System.out.println("Самое длинное слово с минимальным количеством повторений: ");
        tempString = "";
        for (String s : wordsArray) {
            if(s.length() > tempString.length()){
                tempString = s;
            }
        }
        System.out.println(tempString);
    }

    //Вводим строки
    public static void inputText() throws IOException {
        do{tempString = reader.readLine();
            inputTextArray.add(tempString);
        }while (!tempString.equals("end"));
        inputTextArray.remove("end");
    }

    //Находим слово, в котором числоразличных символов минимально
    public static void findWordsWithNonMatches() {
        for (String s : inputTextArray) {
            tempCharsArray = new ArrayList<>();
            int i = 0;
            for (char tmp : s.toCharArray()) {
                tempCharsArray.add(tmp);
            }
            for (char tmp : s.toCharArray()) {
                if (tmp == tempCharsArray.get(i)) {
                    count++;
                }
            }
            i++;
            if (count <= 1) {
                wordsArray.add(s);
            }
            count = 0;
        }
    }
}
