package Lecture2.homework.String;
//4. Ввести n строк с консоли. Найти слово, состоящее только из различных символов. Если таких слов несколько, найти первое из них.

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Task4 {
    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    static ArrayList<String> inputTextArray = new ArrayList<>();
    static ArrayList<String> wordsArray = new ArrayList<>();
    static ArrayList<Character> tempCharsArray = new ArrayList<>();
    static String tempString = "";
    static int count = 0;

    public static void main(String[] args) throws IOException {
        inputText();
        findWordsWithNonMatches();
        System.out.println("Первое слово с уникальными символами: ");
        System.out.println(wordsArray.get(0));
    }

    //Вводим строки
    public static void inputText() throws IOException {
        do {
            tempString = reader.readLine();
            inputTextArray.add(tempString);
        } while (!tempString.equals("end"));
        inputTextArray.remove("end");
    }

    //Находим слово, в котором числоразличных символов минимально
    public static void findWordsWithNonMatches() {
        for (String s : inputTextArray) {
            tempCharsArray = new ArrayList<>();
            int i = 0;
            for (char tmp : s.toCharArray()) {
                tempCharsArray.add(tmp);
            }
            for (char tmp : s.toCharArray()) {
                if (tmp == tempCharsArray.get(i)) {
                    count++;
                }
            }
            i++;
            if (count <= 1) {
                wordsArray.add(s);
            }
            count = 0;
        }
    }
}
