package Lecture2.homework;
//5* Ввести n строк с консоли. Упорядочить и вывести строки в порядке возрастания их длин, а также (второй приоритет) значений этих их длин.
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Task5 {
    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    static ArrayList<String> textArrayList = new ArrayList<>();
    static String [] strArray;
    static String tmp = "";
    static String tmp1;


    public static void main(String[] args) throws IOException {
        inputText();
        convertToArray();
        sortByLength();

    }
    //Вводим n строк
    public static void inputText() throws IOException{
        while (!tmp.equals("q")){
            System.out.println("Введите строку или q для расчета:");
            textArrayList.add(tmp = reader.readLine());
        }

        textArrayList.remove("q");

    }

    public static void convertToArray(){
        for (String s : textArrayList){
            tmp1 = tmp1 + " " + s;
        }
        strArray = tmp1.split(" ");
    }

    //Упорядочиваю и вывожу строки в порядке возростания их длин
    public static void sortByLength(){
        System.out.println("Sort");
        for(int i = 0; i < strArray.length; i++){
            for(int j = 0; j < strArray.length - 1 - i; j++){
                if(strArray[j].length() > strArray[j + 1].length()){
                    String str = strArray[j];
                    strArray[j] = strArray[j + 1];
                    strArray[j + 1] = str;
                }else {}
            }
        }

        for(String s : strArray){
            System.out.println(s + " - " + s.length());
        }
    }
}
