package Lecture2.homework;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Task3 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String inputText = "";
        String string1 = "";
        ArrayList<String> arrayStrings = new ArrayList<>();

        while (!inputText.equals("go")) {
            System.out.println("Введите строку или go для расчета:");
            arrayStrings.add(inputText = reader.readLine());
        }

        arrayStrings.remove("go");
        string1 = arrayStrings.get(0);

        for (String str : arrayStrings) {
            if (string1.length() > str.length()) {
                string1 = str;
            }
        }

        System.out.println("Самая короткая строка: " + string1 + " и ее длинна = " + string1.length());
    }
}
