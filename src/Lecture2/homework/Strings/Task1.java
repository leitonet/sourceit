package Lecture2.homework.Strings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Task1 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String inputText = reader.readLine();
        String [] inputTextArray = inputText.split(" ");
        String [] sentenceArray = inputText.split(".");
        ArrayList <String>  tempArray = new ArrayList<>();
        int countWordsInArray = 0;
        int count = 0;

        //Считаем количество предложений
        for (String s : inputTextArray) {
            if (s.contains(".") || s.contains("!") || s.contains("?")){
                count ++;
            }
        }

        System.out.println("Количество предложений = " + count);

        //
        for(String s : sentenceArray){
            tempArray.add(s);
        }

        for(String s : tempArray){
            countWordsInArray ++;
        }
        System.out.println("Максимальное количество слов в предложении - " + countWordsInArray);

    }
}